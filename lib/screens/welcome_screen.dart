import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:pondit_chatbox/commonwidgets/custombutton.dart';
import 'package:pondit_chatbox/screens/registration_screen.dart';

import 'login_screen.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation animation;

  @override
  void initState() {
    super.initState();
    Firebase.initializeApp();
    controller = AnimationController(
      duration: Duration(microseconds: 500),
      vsync: this,
    );
    animation = ColorTween(begin: Colors.blueGrey, end: Colors.white)
        .animate(controller);
    controller.forward();

    controller.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: animation.value,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Flexible(
              child: Hero(
                tag: 'logo',
                child: Icon(
                  Icons.textsms,
                  size: 120,
                  color: Colors.deepPurple[900],
                ),
              ),
            ),
            SizedBox(
              height: 8.0,
            ),
            Hero(
              tag: 'HeroTitle',
              child: Text(
                'Communication',
                style: TextStyle(
                    color: Colors.deepPurple[900],
                    fontFamily: 'Poppins',
                    fontSize: 26,
                    fontWeight: FontWeight.w700),
              ),
            ),
            TyperAnimatedTextKit(
              isRepeatingAnimation: false,
              speed: Duration(milliseconds: 60),
              text: ["World's most private chatting app".toUpperCase()],
              textStyle: TextStyle(
                  fontFamily: 'Poppins',
                  fontSize: 12,
                  color: Colors.deepPurple),
            ),
            SizedBox(
              height: 48.0,
            ),
            Hero(
              tag: 'loginbutton',
              child: CustomButton(
                text: 'Login',
                accentColor: Colors.white,
                mainColor: Colors.deepPurple,
                onpress: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return LoginScreen();
                  }));
                },
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Hero(
              tag: 'signupbutton',
              child: CustomButton(
                text: 'Signup',
                accentColor: Colors.white,
                mainColor: Colors.lightBlue,
                onpress: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return RegistrationScreen();
                  }));
                },
              ),
            ),
            SizedBox(
              height: 12,
            ),
            Text(
              'Made by Chayti Saha',
              style: TextStyle(fontFamily: 'Poppins'),
            )
          ],
        ),
      ),
    );
  }
}
