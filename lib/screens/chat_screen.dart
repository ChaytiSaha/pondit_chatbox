import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:edge_alert/edge_alert.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';

import '../constants.dart';
import 'login_screen.dart';

final _fireStore = FirebaseFirestore.instance;
User _loggedInUser;

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final _auth = FirebaseAuth.instance;
  String _messageText;
  TextEditingController textEditingController;
  String username;
  String email;

  @override
  void initState() {
    super.initState();
    textEditingController = TextEditingController();
    getCurrentUser();
  }

  void getCurrentUser() async {
    try {
      final user = await _auth.currentUser;
      if (user != null) {
        _loggedInUser = user;
        setState(() {
          username = _loggedInUser.displayName;
          email = _loggedInUser.email;
        });
      }
    } catch (e) {
      EdgeAlert.show(context,
          title: 'Something Went Wrong',
          description: e.toString(),
          gravity: EdgeAlert.BOTTOM,
          icon: Icons.error,
          backgroundColor: Colors.deepPurple[900]);
    }
  }

  void saveMessage(String imageUrl) {
    _fireStore.collection('messages').add({
      'sender': username,
      'text': _messageText,
      'timestamp': DateTime.now().millisecondsSinceEpoch,
      'senderemail': email,
      'imageUrl': imageUrl
    });
  }

  void _sendImageFromGallery() async {
    uploadFile(ImageSource.gallery);
  }

  void _sendImageFromCamera() async {
    uploadFile(ImageSource.camera);
  }

  Future uploadFile(ImageSource imageSource) async {
    final _picker = ImagePicker();
    final PickedFile pickedFile = await _picker.getImage(source: imageSource);
    String filename =
        "chat_" + DateTime.now().millisecondsSinceEpoch.toString() + ".jpg";
    Reference reference =
    FirebaseStorage.instance.ref().child('photos').child(filename);
    uploadFileInFireStorage(pickedFile, reference);
    print('File Uploaded');
  }

  void uploadFileInFireStorage(PickedFile file, Reference ref) async {
    if (file == null) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('No file was selected'),
      ));
      return null;
    } else {
      File image = File(file.path);
      TaskSnapshot uploadTask = await ref.putFile(image);
      if (uploadTask != null) {
        final link = await ref.getDownloadURL();
        setState(() {
          saveMessage(link);
        });
      }
    }
  }

  /* void messagesStream() async {
    await for (var snapshot in _fireStore.collection('messages').snapshots()) {
      for (var message in snapshot.docs) {
        print(message.data());
      }
    }
  }
*/
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.deepPurple),
        elevation: 0,
        bottom: PreferredSize(
          preferredSize: Size(25, 10),
          child: Container(
            child: LinearProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              backgroundColor: Colors.blue[100],
            ),
            constraints: BoxConstraints.expand(height: 1),
          ),
        ),
        backgroundColor: Colors.white10,
        title: Row(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Communication',
                  style: TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: 16,
                      color: Colors.deepPurple),
                ),
                Text('by Chayti Saha',
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        fontSize: 8,
                        color: Colors.deepPurple))
              ],
            ),
          ],
        ),
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              decoration: BoxDecoration(
                color: Colors.deepPurple[900],
              ),
              accountName: Text(username),
              accountEmail: Text(email),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage(
                    "https://cdn.clipart.email/93ce84c4f719bd9a234fb92ab331bec4_frisco-specialty-clinic-vail-health_480-480.png"),
              ),
              onDetailsPressed: () {},
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text("Logout"),
              subtitle: Text("Sign out of this account"),
              onTap: () async {
                await _auth.signOut();
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => LoginScreen()),
                        (route) => false);
              },
            ),
          ],
        ),
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            MessageStream(),
            Container(
              padding: EdgeInsets.only(bottom: 12, top: 8, left: 0, right: 0),
              decoration: kMessageContainerDecoration,
              child: Row(
                children: <Widget>[
                  Container(
                    width: 32,
                    child: IconButton(
                      icon: Icon(
                        Icons.image,
                        color: Colors.blue,
                        size: 30,
                      ),
                      onPressed: _sendImageFromGallery,
                    ),
                  ),
                  Container(
                    width: 32,
                    child: IconButton(
                      icon: Icon(
                        Icons.camera_alt,
                        color: Colors.blue,
                        size: 30,
                      ),
                      onPressed: _sendImageFromCamera,
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: Material(
                      borderRadius: BorderRadius.circular(16),
                      color: Colors.white,
                      elevation: 3,
                      child: Padding(
                        padding:
                        const EdgeInsets.only(left: 8.0, top: 1, bottom: 1),
                        child: TextField(
                          onChanged: (value) {
                            _messageText = value;
                          },
                          controller: textEditingController,
                          decoration: kMessageTextFieldDecoration,
                        ),
                      ),
                    ),
                  ),
                  MaterialButton(
                      shape: CircleBorder(),
                      color: Colors.blue,
                      onPressed: () {
                        textEditingController.clear();
                        saveMessage(null);
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Icon(
                          Icons.send,
                          color: Colors.white,
                        ),
                      )
                    // Text(
                    //   'Send',
                    //   style: kSendButtonTextStyle,
                    // ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MessageBubble extends StatelessWidget {
  final String msgText;
  final String msgSender;
  final String msgImageUrl;
  final bool user;

  MessageBubble({this.msgText, this.msgSender, this.user, this.msgImageUrl});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment:
        user ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: [
          Text(
            msgSender,
            style: TextStyle(fontSize: 12, color: Colors.black87),
          ),
          msgImageUrl == null
              ? Material(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(50),
              topLeft: user ? Radius.circular(50) : Radius.circular(0),
              bottomRight: Radius.circular(50),
              topRight: user ? Radius.circular(0) : Radius.circular(50),
            ),
            color: user ? Colors.blue : Colors.white,
            elevation: 5,
            child: Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 10, horizontal: 20),
                child: Text(
                  msgText,
                  style: TextStyle(
                    color: user ? Colors.white : Colors.blue,
                    fontFamily: 'Poppins',
                    fontSize: 15,
                  ),
                )),
          )
              : Container(
            height: 200,
            width: 200,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(16)),
            child: Image.network(
              msgImageUrl,
              fit: BoxFit.cover,
              height: double.infinity,
              width: double.infinity,
              alignment: Alignment.center,
            ),
          ),
        ],
      ),
    );
  }
}

class MessageStream extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
        stream: _fireStore
            .collection('messages')
            .orderBy('timestamp', descending: true)
            .snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final messages = snapshot.data.docs;
            List<MessageBubble> messageWidgets = [];
            for (var message in messages) {
              final msgText = message.get('text');
              final msgSender = message.get('sender');
              final msgImageUrl = message.get('imageUrl');
              final currentUser = _loggedInUser.displayName;

              // print('MSG'+msgSender + '  CURR'+currentUser);
              final msgBubble = MessageBubble(
                  msgText: msgText,
                  msgSender: msgSender,
                  msgImageUrl: msgImageUrl,
                  user: currentUser == msgSender);
              messageWidgets.add(msgBubble);
            }
            return Expanded(
              child: ListView(
                reverse: true,
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                children: messageWidgets,
              ),
            );
          } else {
            return Container();
          }
        });
  }
}
